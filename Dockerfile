FROM quay.io/eduk8s/base-environment:master

COPY --chown=1001:0 . /home/eduk8s/

# Download and  install cf and kompose binaries
RUN chmod +x /home/eduk8s/workshop/content/installs/* && \
    mv /home/eduk8s/workshop/content/installs/* /opt/eduk8s/bin && \
    mv /home/eduk8s/workshop /opt/workshop

RUN fix-permissions /home/eduk8s
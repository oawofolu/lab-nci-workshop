### Updating Docker Images

First, we will update the Docker images to ensure that they will run in a Kubernetes cluster.

Clone the application's git repository: 
```execute
git clone {{ app-git-repo }}
```
**cd** to the cloned apps' root  directory:
```execute
cd ~/{{ app-git-repo-clone-dir }}
```

View the **Dockerfile** for the app:
```editor:open-file
file: ~/nci-lant/Dockerfile
```
### Workshop Overview

The **Tanzu platform** allows developers to deploy applications and services to public, private and hybrid cloud environments using open source APIs/CLIs. 

Developers can choose to deploy their applications to **Tanzu Kubernetes Grid** (TKG) or **Tanzu Application Service** (TAS). Choosing the right target for an application depends on the use case. In general, TAS is the fast-track approach for deploying apps to the cloud, and is suitable for apps that have a higher degree of cloud native maturity -  see [12factor.net](https://12factor.net). TKG requires more effort to deploy to the cloud, but is more flexible and accomodating of apps with different architectures and packaging.

![Deploying App](images/deploymentpaths.png)

In this workshop, you will learn how to deploy your Django application to Tanzu Kubernetes Grid Integrated (TKGi). Later, you will learn how to deploy the same application to TAS.

Let's get started!